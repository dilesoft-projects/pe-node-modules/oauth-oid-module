export default async function (auth_req_id, expires_in, interval, state) {
  return {
    auth_req_id,
    expires_in,
    interval,
    state,
  };
}
